package com.admatic.facebook;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.admatic.utils.ApplicationUtils;
import com.admatic.utils.MessageUtils;

public class FBGraph {
	private String accessToken;

	public FBGraph(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public String setProduct(String catalogId) {
		String graph = null;
		try {

			//String params = "brand=Marca XYZ&category=Categoria 1&currency=Real&description=Uma descricao qualquer&image_url=http://www.lampp.com.br/images/areasatuacao/ensino_23414.png&name=Produto 1&price=599&retailer_id=abc123&url=http://lampp.com.br";
			String g = "https://graph.facebook.com/v2.7/" + catalogId + "/products";
			URL u = new URL(g);
			
			HashMap<String, String> params = new HashMap<String, String>();
	        params.put("brand", "Marca XYZ");
	        params.put("category", "Tenis");
	        params.put("currency", "BRL");
	        params.put("description", "Uma descricao qualquer");
	        params.put("image_url", "https://www.lampp.com.br/images/areasatuacao/ensino_23414.png");
	        params.put("name", "Produto 1");
	        params.put("price", "599");
	        params.put("retailer_id", "abc123");
	        params.put("url", "http://www.lampp.com.br");	   	
			
			StringBuilder postData = new StringBuilder();
			
	        for (Map.Entry<String, String> param : params.entrySet()) {
	            if (postData.length() != 0) {
	                postData.append('&');
	            }
	            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
	            postData.append('=');
	            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
	        }			
			
	        postData.append("&").append(accessToken);
	        
	        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
	        
	        HttpURLConnection c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("POST");
			c.setRequestProperty( "Content-type", "application/x-www-form-urlencoded");
	        c.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));	       			
			c.setRequestProperty( "Accept", "*/*" );
			
			c.setDoOutput(true);
	        c.getOutputStream().write(postDataBytes);
	        
			BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
			String inputLine;
			StringBuffer b = new StringBuffer();
			while ((inputLine = in.readLine()) != null)
				b.append(inputLine + "\n");
			in.close();
			graph = b.toString();
			System.out.println(graph);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(MessageUtils.MSG_GRAPH_DATA_ERROR + e);
		}
		return graph;
	}

	public String getFBGraph() {
		String graph = null;
		try {

			String g = ApplicationUtils.URL_GRAPH + accessToken;
			URL u = new URL(g);
			URLConnection c = u.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					c.getInputStream()));
			String inputLine;
			StringBuffer b = new StringBuffer();
			while ((inputLine = in.readLine()) != null)
				b.append(inputLine + "\n");
			in.close();
			graph = b.toString();
			System.out.println(graph);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(MessageUtils.MSG_GRAPH_DATA_ERROR + e);
		}
		return graph;
	}

	public Map<String, String> getGraphData(String fbGraph) {
		Map<String, String> fbProfile = new HashMap<String, String>();
		try {
			JSONObject json = new JSONObject(fbGraph);
			fbProfile.put("id", json.getString("id"));
			fbProfile.put("name", json.getString("name"));
		} catch (JSONException e) {
			e.printStackTrace();
			throw new RuntimeException(MessageUtils.MSG_PARSING_ERROR + e);
		}
		return fbProfile;
	}
}
