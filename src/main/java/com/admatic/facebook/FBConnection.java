package com.admatic.facebook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import com.admatic.utils.ApplicationUtils;
import com.admatic.utils.MessageUtils;

public class FBConnection {
		
	static String accessToken = "";

	public static String getFBAuthUrl() {
		String fbLoginUrl = "";
		try {
			fbLoginUrl = "http://www.facebook.com/dialog/oauth?" + "client_id="
					+ ApplicationUtils.FB_APP_ID + "&redirect_uri="
					+ URLEncoder.encode(ApplicationUtils.REDIRECT_URI, "UTF-8")
					+ "&scope=" + ApplicationUtils.SCOPE;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return fbLoginUrl;
	}

	public static String getFBGraphUrl(String code) {
		String fbGraphUrl = "";
		try {
			fbGraphUrl = "https://graph.facebook.com/oauth/access_token?"
					+ "client_id=" + ApplicationUtils.FB_APP_ID + "&redirect_uri="
					+ URLEncoder.encode(ApplicationUtils.REDIRECT_URI, "UTF-8")
					+ "&client_secret=" + ApplicationUtils.FB_APP_SECRET + "&code=" + code;
			System.out.println(fbGraphUrl);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return fbGraphUrl;
	}

	public static String getAccessToken(String code) {
		if ("".equals(accessToken)) {
			URL fbGraphURL;
			try {
				fbGraphURL = new URL(getFBGraphUrl(code));
			} catch (MalformedURLException e) {
				e.printStackTrace();
				throw new RuntimeException(MessageUtils.MSG_INVALID_CODE + e);
			}
			URLConnection fbConnection;
			StringBuffer b = null;
			try {
				fbConnection = fbGraphURL.openConnection();
				BufferedReader in;
				in = new BufferedReader(new InputStreamReader(
						fbConnection.getInputStream()));
				String inputLine;
				b = new StringBuffer();
				while ((inputLine = in.readLine()) != null)
					b.append(inputLine + "\n");
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(MessageUtils.MSG_CONNECTION_ERROR
						+ e);
			}

			accessToken = b.toString();
			if (accessToken.startsWith("{")) {
				throw new RuntimeException(MessageUtils.MSG_INVALID_ACCESS_TOKEN
						+ accessToken);
			}
		}
		return accessToken;
	}
}