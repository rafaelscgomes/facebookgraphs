package com.admatic.workers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admatic.producers.FacebookApiProducer;
import com.admatic.rabbitmq.ApiProducer;
import com.admatic.rabbitmq.workers.ApiWorker;

public class FacebookCollectionsApiWorker extends ApiWorker{

	private static Logger LOG = LoggerFactory.getLogger(FacebookCollectionsApiWorker.class);
	
	private static final String QUEUE = "facebook_collection_api";

	public FacebookCollectionsApiWorker(String exchange) throws IOException {
		super(exchange);
	}
	
	public FacebookCollectionsApiWorker() throws IOException {
		super();
	}
	
	private ApiProducer producer;

	public static void main(String[] args) throws IOException {
		List<String> params = Arrays.asList(args);

		int threadsCount = params.indexOf("--threads") > -1 ? Integer.parseInt(params.get(params.indexOf("--threads") + 1)) : 5;

		System.out.printf("Total threads: %d\n", threadsCount);

		Thread[] threads = new Thread[threadsCount];

		for (int i = 0; i < threadsCount; i++) {

			threads[i] = new Thread(new Runnable() {

				@Override
				public void run() {
					try(ApiProducer buscapeProducer = new FacebookApiProducer()) {
						FacebookCollectionsApiWorker worker = new FacebookCollectionsApiWorker();
						worker.setProducer(buscapeProducer);
						worker.startWorker(worker.getChannel(), QUEUE, QUEUE);
					} catch (Exception e) {
						LOG.error("Couldn't start FacebookApiWorker {}", e);
					}
				}
			});

			threads[i].start();
		}
	}

	public void setProducer(ApiProducer buscapeProducer) {
		producer = buscapeProducer;
	}

	public ApiProducer getProducer(){
		return producer;
	}

	public String getAction() {
		return ApiProducer.COLLECTION_MODE;
	}

}
