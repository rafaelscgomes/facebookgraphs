package com.admatic.workers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admatic.producers.FacebookApiProducer;
import com.admatic.rabbitmq.ApiProducer;
import com.admatic.rabbitmq.workers.ApiWorker;

public class FacebookInventoryApiWorker extends ApiWorker {
	private static Logger LOG = LoggerFactory.getLogger(FacebookInventoryApiWorker.class);
	
	private static final String QUEUE = "facebook_inventory_api";
	
	public FacebookInventoryApiWorker(String exchange) throws IOException {
		super(exchange);
	}
	
	public FacebookInventoryApiWorker() throws IOException {
		super();
	}

	private ApiProducer producer;
	
	public static void main(String[] args) throws IOException {
		List<String> params = Arrays.asList(args);

		int threadsCount = params.indexOf("--threads") > -1 ? Integer.parseInt(params.get(params.indexOf("--threads") + 1)) : 5;

		System.out.printf("Total threads: %d\n", threadsCount);

		Thread[] threads = new Thread[threadsCount];

		for (int i = 0; i < threadsCount; i++) {

			threads[i] = new Thread(new Runnable() {

				@Override
				public void run() {
					try(ApiProducer buscapeProducer = new FacebookApiProducer()) {
						FacebookInventoryApiWorker worker = new FacebookInventoryApiWorker();
						worker.setProducer(buscapeProducer);
						worker.startWorker(worker.getChannel(), QUEUE, QUEUE);
					} catch (Exception e) {
						LOG.error("Couldn't start BuscapeInventoryApiWorker {}", e);
					}
				}
			});

			threads[i].start();
		}
	}

	public void setProducer(ApiProducer buscapeProducer) {
		producer = buscapeProducer;
	}

	public ApiProducer getProducer(){
		return producer;
	}

	public String getAction() {
		return ApiProducer.INVENTORY_MODE;
	}
}

