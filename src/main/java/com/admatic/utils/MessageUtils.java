package com.admatic.utils;


public class MessageUtils {
	
	public static final String MSG_INVALID_CODE ="ERROR: Invalid code received.";
	public static final String MSG_PARSING_ERROR = "Error in parsing FB graph data. ";
	public static final String MSG_GRAPH_DATA_ERROR = "Error in getting FB graph data. ";
	public static final String MSG_CONNECTION_ERROR = "Unable to connect with Facebook ";
	public static final String MSG_INVALID_ACCESS_TOKEN = "ERROR: Access Token Invalid: ";

}
