package com.admatic.producers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.Header;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admatic.beans.ApiConnection;
import com.admatic.db.impl.mongodb.ApiConnectionDataSource;
import com.admatic.facebook.FBConnection;
import com.admatic.mail.EmailSender;
import com.admatic.mail.EmailSender.EmailRecipient;
import com.admatic.rabbitmq.ApiProducer;
import com.admatic.scripts.b2w.OfferApiCall;
import com.admatic.utils.ApplicationUtils;
import com.admatic.utils.RequestBridge;
import com.mongodb.BasicDBObject;

public class FacebookApiProducer extends ApiProducer{
	private static final Logger LOG = LoggerFactory.getLogger(FacebookApiProducer.class);

	public static final String API_URL = "https://graph.facebook.com/v2.7/";
	
	public static void main(String[] args) {
		try (ApiProducer producer = new FacebookApiProducer()) {
			LOG.debug("Invoking FacebookApiProducer");
			producer.invoke(args);
		} catch (Exception e) {
			LOG.error("{}", e);
		}
		System.exit(1);
	}


	public FacebookApiProducer(String action) {
		super(action);
	}
	
	public FacebookApiProducer() {
		super("");
	}

	@Override
	protected BasicDBObject createCollectionMessage(Map<String, Object> item) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("retailer_id", "Y2JhMTIz");
		map.put("brand", "Marca XYZ");
		map.put("category", "Tenis");
		map.put("currency", "BRL");
		map.put("description", "Uma descricao qualquer");
		map.put("image_url", "https://www.lampp.com.br/images/areasatuacao/ensino_23414.png");
        map.put("name", "Produto 1");
        map.put("price", "599");
        map.put("url", "http://www.lampp.com.br");
		
		
		return new BasicDBObject(item);
	}

	public Boolean validate(String token) {
		return validation(token);
	}

	@Override
	protected Boolean validation(String token) {

		try {
			Header[] headers = {
					new Header("Content-Type", "application/json"),
					new Header("Accept", "application/json"),
					new Header("auth-token", token),
			};
			Map<String, Object> item = new HashMap<>();
			item.put("brand", "Marca XYZ");
			item.put("category", "Tenis");
			item.put("currency", "BRL");
			item.put("description", "Uma descricao qualquer");
	        item.put("image_url", "https://www.lampp.com.br/images/areasatuacao/ensino_23414.png");
	        item.put("name", "Produto 1");
	        item.put("price", "599");
	        item.put("retailer_id", "abc123");
	        item.put("url", "http://www.lampp.com.br");

			List<Map<String, Object>> list = new ArrayList<>();
			list.add(createInventoryMessage(item));

			final String payload = mapper.writeValueAsString(list);

			int response = RequestBridge.getUrlStatusCode(API_URL+ "199639713783536" + "/products",
					COLLECTION_MODE.equals(getAction()) ? RequestBridge.POST_METHOD : RequestBridge.PUT_METHOD,
					"UTF-8",
					payload,
					true,
					headers);

			System.out.println(response);

			if(response != ApplicationUtils.NOT_AUTHORIZED){
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	protected BasicDBObject createInventoryMessage(Map<String, Object> item) {
		int quantity = isAvailable(item) ? 1 : 0;

		BasicDBObject message = new BasicDBObject();
		message.put("name", item.get("name"));
		message.put("quantity", quantity);

		return message;
	}

	@Override
	protected boolean updateInventoryOnApi(ApiConnection apiConnection, List<BasicDBObject> list) {
		return callApi(apiConnection, list, API_URL, false);
	}

	@Override
	protected boolean updateCollectionOnApi(ApiConnection apiConnection, List<BasicDBObject> list) {
		return callApi(apiConnection, list, API_URL, false);
	}

	public Map<String, Object> getProducts(ApiConnection apiConnection, int page) {
		String url = API_URL;

		if (page != 0) {
			url = String.format("%s?size=30&page=%d", url, page);
		}
		else {
			url = String.format("%s?size=30", url);
		}

		return getProducts(apiConnection, url);
	}

	@Override
	public Map<String, Object> getProducts(ApiConnection apiConnection, List<BasicDBObject> list) {
		String url = API_URL;

		if (list != null && list.size() == 1) {
			url += "/" + list.get(0).getString("original_id");
		}

		return getProducts(apiConnection, url);
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getProducts(ApiConnection apiConnection, String url) {
		String payload = null;
		try {
			if (apiConnection.getAccountId() == 3) {
				return null;
			}

			String authToken = apiConnection.getToken();

			Header[] headers = {
					new Header("Content-Type", "application/json"),
					new Header("Accept", "application/json"),
					new Header("app-token", "199639713783536"),
					new Header("auth-token", authToken),
			};

			Map<String, Object> response = RequestBridge.getUrlReturn(url, RequestBridge.GET_METHOD, "UTF-8", payload, true, headers);

			String body = (String) response.get("body");

			Map<String, Object> result = mapper.readValue(body, Map.class);

			return result;

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error calling Buscape API: {}", e);
		}
		return null;
	}
	
	private boolean callApi(ApiConnection apiConnection, List<BasicDBObject> list, String apiUrl, boolean retry) {
		String payload = null;
		String body = null;
		String statusCode = null;
		try {

			String authToken = apiConnection.getToken();

			Header[] headers = {
					new Header("Content-Type", "application/json"),
					new Header("Accept", "application/json"),
					new Header("app-token", "199639713783536"),
					new Header("auth-token", authToken),
			};

			payload = mapper.writeValueAsString(list);
			
			
			Map<String, Object> auth = RequestBridge.getUrlReturn(FBConnection.getFBAuthUrl(), RequestBridge.GET_METHOD);
			
			Map<String, Object> response = RequestBridge.getUrlReturn(apiUrl+apiConnection.getToken()+"/products",
					COLLECTION_MODE.equals(getAction()) ? RequestBridge.POST_METHOD : RequestBridge.PUT_METHOD,
					"UTF-8",
					payload,
					true,
					headers);

			body = (String) response.get("body");
			statusCode = String.valueOf(response.get("statusCode"));

			System.out.println(payload);
			System.out.println(body);

			this.log(list, body);
			boolean success = response.get("statusCode").equals(200);

			if (success) {
				apiConnection.setLastSuccessfulCall(new Date());
			}

			return success;
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error calling Facebook API: {}", e);
			StringBuilder mailBody = new StringBuilder();

			for (BasicDBObject offer: list) {
				mailBody.append("- ").append(offer.getString("sku")).append("<br />");
			}

			mailBody.append(ExceptionUtils.getStackTrace(e));

			mailBody.append("<br/>Payload: ").append(payload);

			mailBody.append("<br/>Response: ").append(body);
			mailBody.append("<br/>Status Code: ").append(statusCode);

			EmailSender.sendMail("Erro enviando ofertas para o Buscap� - "+getAccountId(), mailBody.toString(),
					Collections.<EmailRecipient>singletonList(new EmailRecipient("dev@admatic.com.br", "Equipe dev")), null);

			return false;
		}
		finally {
			apiConnection.setLastCall(new Date());
			ApiConnectionDataSource.getInstance().insertOrUpdate(apiConnection);
		}
	}
	
	


	@Override
	protected void populateApiLog(List<OfferApiCall> apiLogs, Object list,
			Object response) throws JSONException {
		
	}

	@Override
	public String getChannel() {
		return "facebook";
	}
	
	

}
